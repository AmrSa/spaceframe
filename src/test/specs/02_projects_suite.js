import AllureReporter from '@wdio/allure-reporter';
import l from '../pages/loginPage';
import p from '../pages/adminDashboardPage';
import D from '../../test/utils/data-provider';
import h from '../../test/utils/helper';
import 'mocha';

describe('Login Test Suite', () => {
    before(() => {
        browser.url('/spaceframe/login');
        l.loginHeader.waitForDisplayed();
        l.loginWith('spaceframe', 'spac3##');
    });

    it('01. Should be able to add new project', () => {
        AllureReporter.addSeverity('');
        AllureReporter.addStory('');
        AllureReporter.addDescription('');
        p.fillProjectDetailsFields(D.projectNumber, D.qouteId, D.projectName, D.directory);
        browser.expectListOfElementsToBeDisplayed(
            [
                p.genricEl(D.qouteId), p.genricEl(D.type), p.genricEl(D.apps), p.genricEl(D.projectPrincipal),
                p.genricEl(D.principalABN), p.genricEl(D.principalAddress),
                p.genricEl(D.principalContactName), p.genricEl(D.principalContactEmail),
                p.genricEl(D.principalContactMobile), p.genricEl(D.superName), p.genricEl(D.superEmail), p.genricEl(D.superMobile)
            ]);
    });
});
