import AllureReporter from '@wdio/allure-reporter';
import p from '../pages/loginPage';
import D from '../utils/data-provider';
import DB from '../utils/DB-utils';

describe('Login Test Suite', () => {
    let user_id;
    after(()=>{
        DB.deleteUserRecord(user_id)
    })
    context('Username and Pass Validation', () => {
        D.parseCSVFileToJSON('src/test/resources/list.csv', 'utf8').forEach((input, i) => {
            let testName = `${i + 1}. Login with ${input.username} & ${input.password}`;
            testName += ` should show error: ${input.expectedError}`;
            it(testName, () => {
                AllureReporter.addSeverity('critical');
                AllureReporter.addStory(`user should not be able to ${testName}`);
                AllureReporter.addDescription(`should get the following error msg: ${input.expectedError}}`);
                browser.url('/spaceframe/login');
                p.loginWith(input.username, input.password)
                    .waitForErrorMsg();
                expect(p.errorMsg).toHaveText(input.expectedError);
            });
        });
    });

    before(() => {
        browser.url('/spaceframe/login');
        p.loginHeader.waitForDisplayed();
    });

    it('01. Should not be able to login with invalid cred', () => {
        AllureReporter.addSeverity('critical');
        AllureReporter.addStory('user should not be able to login with invalid cred');
        AllureReporter.addDescription('when credentials are invalid, error msg should pop up Invalid credentials specified');
        p.loginWith('spaceframe', 'anything')
            .waitForErrorMsg();
        expect(p.errorMsg).toHaveText('Invalid credentials specified');
    });

    it('02. should not be able to login without username', () => {
        AllureReporter.addSeverity('critical');
        AllureReporter.addStory('user should not be able to login without username');
        AllureReporter.addDescription('when no username is provided, error msg Please specify a username');
        p.loginWith('', 'anything')
            .waitForErrorMsg();
        expect(p.errorMsg).toHaveText('Please specify a username');
    });

    it('03. Should not be able to login without password', () => {
        AllureReporter.addSeverity('critical');
        AllureReporter.addStory('user should not be able to login without passowrd');
        AllureReporter.addDescription('when no password is provided, error msg Please specify a password');
        p.loginWith('spaceframe', '')
            .waitForErrorMsg();
        expect(p.errorMsg).toHaveText('Please specify a password');
    });

    it('04. Should not be able to login without credentials', () => {
        AllureReporter.addSeverity('critical');
        AllureReporter.addStory('user should not be able to login without username and passowrd');
        AllureReporter.addDescription('when no credentials are provided, error msg No username or password specified');
        p.loginWith('', '')
            .waitForErrorMsg();
        expect(p.errorMsg).toHaveText('No username or password specified');
    });

    context.skip('05. Forget password Tests', () => {
        before(() => {
            p.forgetPassword.waitForDisplayed();
            p.forgetPassword.click();
        });

        it('05-1. should get an error when no email is provided', () => {
            AllureReporter.addSeverity('critical');
            AllureReporter.addStory('user should not be able to reset password without an email');
            AllureReporter.addDescription('Error message will pop up');
            p.forgetPasswordOfInvalid('');
            expect(p.emailError).toHaveText('The email you have entered does not exist in our system');
        });

        it('05-2. should get error when invalid email is provided', () => {
            AllureReporter.addSeverity('critical');
            AllureReporter.addStory('user should not be able to reset password for invalid email');
            AllureReporter.addDescription('Error message will pop up');
            p.forgetPasswordOfInvalid('');
            expect(p.emailError).toHaveText('The email you have entered does not exist in our system');
        });

        it('05-3. alert window should pop up when providing valid email', () => {
            AllureReporter.addSeverity('critical');
            AllureReporter.addStory('user should be able to reset password for valid email');
            AllureReporter.addDescription('alet window will pop up');
            p.forgetPasswordOf('info@company.com')
                .waitForAlertWindowToBeDisplayed();
            expect(browser.getAlertText()).toEqual('An email has been sent. Please check your inbox to reset your password.');
        });
    });

    it('01. Get username from DB and login with it', () => {
        const output = browser.submitDBQuery('select * from spaceframe_white.sb__user where id=1')
        // const output = DB.submitDBQuery('select * from spaceframe_white.sb__user where id=1')
        const username = output[0].username
        // const username = results[0].username
        p.loginWith(username,'spac3##');
    });

    it.only('01. Get username from DB and login with it', () => {
        user_id = DB.insertNewUser(D.superName,D.superEmail)[0].insertId
        DB.addPass(user_id)
        p.loginWith(D.superName,D.superEmail);
        browser.pause(5000)
    });
    
});
