import AllureReporter from '@wdio/allure-reporter';

class LoginPage {
    /* =====================================================================  ELEMENTS ============================================================================== */

    get loginHeader () { return $('h4=Enter your details to login to the system'); }
    get username () { return $('#username'); }
    get password () { return $('#password'); }
    get loginButton () { return $('#submit_login_form'); }
    get errorMsg () { return $('#login_error'); }
    get forgetPassword () { return $('//*[contains(text(),"Forgot password?")]'); }
    get resetLink () { return $('#sub'); }
    get email () { return $('#email'); }
    get emailError () { return $('#email-error'); }
    get () { return $(''); }
    get () { return $(''); }

    /* ========================================================================= ACTIONS ================================================================================ */

    loginWith (username, password) {
        this.username.setValue(username);
        this.password.setValue(password);
        this.loginButton.click();
        return this;
    }

    waitForErrorMsg () {
        this.errorMsg.waitForDisplayed();
        return this;
    }

    forgetPasswordOfInvalid (email) {
        this.email.setValue(email);
        this.resetLink.click();
        this.emailError.waitForDisplayed({ timeout: 2000 });
        return this;
    }

    forgetPasswordOf (email) {
        this.email.setValue(email);
        this.resetLink.click();
        return this;
    }

    waitForAlertWindowToBeDisplayed () {
        browser.waitUntil(() => {
            try {
                browser.getAlertText();
                return true;
            } catch (err) {
                return false;
            }
        });
        return this;
    }
}

export default new LoginPage();
