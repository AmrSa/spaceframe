import AllureReporter from '@wdio/allure-reporter';
import D from '../utils/data-provider';

class AdminDashboard {
    /* =====================================================================  ELEMENTS ============================================================================== */

    get adminDashboard () { return $('h2=Admin dashboard'); }
    get projectLink () { return $('a=Projects'); }
    get addProject () { return $('a=Add project'); }
    get projectNumber () { return $('#project_code'); }
    get QuoteId () { return $('#quote_id'); }
    get projectName () { return $('#name'); }
    get typeMenu () { return $$('.search')[1]; } // index 1
    dropMenuItem (items) { return $('div=' + items); }
    get principal () { return $('#principle'); }
    get principalAbn () { return $('#principle_abn'); }
    get principalAddress () { return $('#principle_address'); }
    get localGov () { return $('#local_government'); }
    get designAppsMenu () { return $$('.search')[3]; } // index 3
    get principalContactName () { return $('#principal_contact_name'); }
    get principalContactEmail () { return $('#principal_contact_email'); }
    get principalContactMobile () { return $('#principal_contact_mobile'); }
    get superIntendentName () { return $('#superintendent_name'); }
    get superIntendedEmail () { return $('#superintendent_email'); }
    get superIntendentMobile () { return $('#superintendent_mobile'); }
    get description () { return $('#description'); }
    get siteManager () { return $('#site_manager'); }
    get sitManagerEmail () { return $('#site_manager_email'); }
    get constManagerMenu () { return $$('.search')[5]; } // index 5
    get contractsAdminMenu () { return $$('.search')[7]; } // index 7
    get drafteeUserMenu () { return $$('.search')[9]; } // index 9
    get projectTeamMenu () { return $$('.search')[11]; } // index 11
    get pcDate () { return $('#pc_date'); }
    get docName () { return $('#doctor_name'); }
    get docAddress () { return $('#doctor_address'); }
    get docPhone () { return $('#doctor_phone'); }
    get hospitalName () { return $('#hospital_name'); }
    get hospitalAddress () { return $('#hospital_address'); }
    get hospitalPhone () { return $('#hospital_phone'); }
    get startDate () { return $('#start_date'); }
    get constructionPeriod () { return $('#construction_period'); }
    get csmp () { return $('#csmp'); }
    get msm () { return $('#msm'); }
    get contractTypeMenu () { return $$('.search')[14]; }
    get statusMenu () { return $$('.search')[16]; }
    get contractAmount () { return $('#contract_expense'); }
    get siteAddress () { return $('#street'); }
    get stateMenu () { return $$('.search')[18]; }
    get postCode () { return $('#postcode'); }
    get ldsDay () { return $('#lds_day'); }
    get dlpContract () { return $('#dlp_contract'); }
    get dlpDate () { return $('#dlp_date'); }
    get insertPDF () { return $('label=Yes - (Insert PDF into Annexure G)'); }
    get saveDetail () { return $('#submit_btn'); }
    get cancelGoBack () { return $('a=Cancel and go back'); }
    projectHeader (projectHead) { return $('h1=' + projectHead); }
    allProjectDetails (info) { return $('tc=' + info); }
    genricEl (value) { return $('td=' + value); }
    get () { return $(''); }
    get () { return $(''); }
    get () { return $(''); }

    /* ========================================================================= ACTIONS ================================================================================ */

    fillProjectDetailsFields (projectNumber, quoteId, projectName) {
        this.adminDashboard.waitForDisplayed();
        this.projectLink.click();
        this.addProject.waitForDisplayed();
        this.addProject.click();
        this.projectNumber.setValue(projectNumber);
        this.QuoteId.setValue(quoteId);
        this.projectName.setValue(projectName);
        this.typeMenu.click();
        this.dropMenuItem(D.type).click();
        this.principal.setValue(D.projectPrincipal);
        this.principalAbn.setValue(D.principalABN);
        this.principalAddress.setValue(D.principalAddress);
        this.localGov.setValue(D.localGov);
        this.designAppsMenu.click();
        this.dropMenuItem(D.apps).waitForDisplayed();
        this.dropMenuItem(D.apps).click();
        this.principalContactName.setValue(D.principalContactName);
        this.principalContactEmail.setValue(D.principalContactEmail);
        this.principalContactMobile.setValue(D.principalContactMobile);
        this.superIntendentName.setValue(D.superName);
        this.superIntendedEmail.setValue(D.superEmail);
        this.superIntendentMobile.setValue(D.superMobile);
        this.description.setValue(D.description);
        this.siteManager.setValue(D.siteManager);
        this.sitManagerEmail.setValue(D.siteManagerEmail);
        this.constManagerMenu.click();
        this.dropMenuItem(D.constructionManager).waitForDisplayed();
        this.dropMenuItem(D.constructionManager).click();
        this.contractsAdminMenu.click();
        this.dropMenuItem(D.admin).waitForDisplayed();
        this.dropMenuItem(D.admin).click();
        this.drafteeUserMenu.click();
        this.dropMenuItem(D.DrafteeUser).waitForDisplayed();
        this.dropMenuItem(D.DrafteeUser).click();
        this.projectTeamMenu.click();
        this.dropMenuItem(D.teamMember).waitForDisplayed();
        this.dropMenuItem(D.teamMember).forceClick();
        this.docName.setValue(D.doctorName);
        this.docAddress.setValue(D.doctorAddress);
        this.docPhone.setValue(D.doctorPhone);
        this.hospitalName.setValue(D.hospitalName);
        this.hospitalAddress.setValue(D.hospitalAddress);
        this.hospitalPhone.setValue(D.hospitalPhone);
        this.constructionPeriod.setValue(D.constructionPeriod);
        this.csmp.setValue(D.csmp);
        this.msm.setValue(D.msm);
        this.contractTypeMenu.click();
        this.dropMenuItem(D.contractType).waitForDisplayed();
        this.dropMenuItem(D.contractType).click();
        this.statusMenu.click();
        this.dropMenuItem(D.status).waitForDisplayed();
        this.dropMenuItem(D.status).click();
        this.contractAmount.setValue(D.contractAmount);
        this.siteAddress.setValue(D.siteAddress);
        this.stateMenu.click();
        this.dropMenuItem(D.state).waitForDisplayed();
        this.dropMenuItem(D.state).click();
        this.postCode.setValue(D.postcode);
        this.ldsDay.waitForDisplayed();
        this.ldsDay.setValue(D.lds);
        this.dlpContract.setValue(D.dlp);
        this.saveDetail.click();
        return this;
    }
}

export default new AdminDashboard();
