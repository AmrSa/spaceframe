import frisby from 'frisby';

const baseUrl = 'https://reqres.in/api';

class API {
    generic_api_request = (suffix, method, requestBody, expectedStatus) => {
        if (method === 'get') {
            return frisby
                .get(`${baseUrl}/${suffix}`)
                .expect('status', expectedStatus);
        } else {
            return frisby
                .post(`${baseUrl}/${suffix}`, requestBody)
                .expect('status', expectedStatus);
        }
    };
}

export default new API();
