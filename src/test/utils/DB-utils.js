const D = require ('../utils/data-provider');
import mysql from 'mysql2';
import {Client} from 'ssh2';

const sshClient = new Client();
const dbServer = D.dbServer
const tunnelConfig = D.tunnelConfig
const forwardConfig = D.forwardConfig

let pool;

class DB {
    async main() {
        new Promise((resolve, reject) => {
            sshClient.on('ready', () => {
                sshClient.forwardOut(
                    forwardConfig.srcHost,
                    forwardConfig.srcPort,
                    forwardConfig.dstHost,
                    forwardConfig.dstPort,
                    (err, stream) => {
                        if (err) reject(err);
                        // create a new DB server object including stream
                        const updatedDbServer = {
                            ...dbServer,
                            stream
                        };            // connect to mysql
                        pool = mysql.createPool(updatedDbServer);
                    });
            }).connect(tunnelConfig);
        });
    }

    submitDBQuery(query) {
        return browser.call(async () => {
            const promisePool = pool.promise();
            const outPut = await promisePool.query(query);
            const results = JSON.parse(JSON.stringify(outPut))
            return await results[0]
        })
    }

    insertNewUser(username,email) {
        return browser.call(async () => {
            const promisePool = pool.promise();
            const outPut = await promisePool
                .query(`INSERT  INTO spaceframe_white.sb__user (username, email, title, first_name, last_name, is_active, is_admin)
          VALUES ('${username}','${email}','Mr','automation','test',1,1);`);
            const results = JSON.parse(JSON.stringify(outPut))
            console.log(results)
            return await results
        })
    }

    getUserId() {
        return browser.call(async () => {
            const promisePool = pool.promise();
            const outPut = await promisePool
                .query(`SELECT * FROM spaceframe_white.sb__user WHERE username ='amrx'`);
            const results = JSON.parse(JSON.stringify(outPut))
            console.log('thereeeeeeeeeeee')
            return await results[0]
        })
    }

    addPass(user_id) {
        return browser.call(async () => {
            const promisePool = pool.promise();
            const outPut = await promisePool
                .query(`INSERT INTO spaceframe_white.sb__user_password (user_id, password, is_current) 
                VALUES (${user_id},'$2y$10$.AKVO5LvLiqoM64vfQC.d.U05DU3tXiiGlr0FULJTVVfzlWYsll4G',1);`);
            const results = JSON.parse(JSON.stringify(outPut))
            return await results
        })
    }

    deleteUserRecord(user_id) {
        return browser.call(async () => {
            const promisePool = pool.promise();
            const outPut = await promisePool
                .query(
                    `DELETE FROM spaceframe_white.sb__user_password WHERE user_id=${user_id};
                     DELETE FROM spaceframe_white.sb__user WHERE id=${user_id};`);
            const results = JSON.parse(JSON.stringify(outPut))
            return await results
        })
    }

}

export default new DB();
