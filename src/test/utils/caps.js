const caps = exports;

caps.ChromeDesktopCaps =
[{
    port: 4455,
    path: '/wd/hub',
    browserName: 'chrome',
    acceptInsecureCerts: true,
    'goog:chromeOptions': {
        args: [
            '--disable-dev-shm-usage',
            '--no-sandbox',
            '--enable-automation',
            '--disable-gpu',
            '--window-size=1920,1080',
            // '--headless',
        ],
    }
}];

module.exports = caps;
