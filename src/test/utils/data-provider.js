const fs = require('fs');
require('mysql2')
const D = exports;
const helper = require('../utils/helper');

D.dbServer = {
    host: 'localhost',
    port: 3306,
    user: 'spaceframe_white',
    password: 'SF_WHITE',
    waitForConnections: true,
    connectionLimit: 10,
    rowsAsArray: false,
    multipleStatements: true
}

D.tunnelConfig = {
    host: '159.89.208.60',
    port: 22,
    username: 'root',
    password: 'S3ns0rP@$$'
}
D.forwardConfig = {
    srcHost: 'localhost',
    srcPort: 3306,
    dstHost: D.dbServer.host,
    dstPort: D.dbServer.port
};

D.UsersList = {
    validUser: {
        userName: 'spaceframe',
        passWord: 'spac3##'
    },
    InvalidUser: {
        userName: '',
        passWord: ''
    }
};

D.inputs = [
    {
        username: 'spaceframe',
        password: 'anything',
        expectedError: 'Invalid credentials specified'
    },
    {
        username: '',
        password: 'anything',
        expectedError: 'Please specify a username'
    },
    {
        username: 'spaceframe',
        password: '',
        expectedError: 'Please specify a password'
    },
    {
        username: '',
        password: '',
        expectedError: 'No username or password specified'
    }
];

const setNewRandomNo = function (int) {
    return Math.floor(int * Math.random() + 1).toString();
};
const RandomString = helper.getRandomString(5) + ` ${setNewRandomNo(9000)}`;

const getRandomString = async (length)=> {
    
    // new Promise(function(resolve, reject) {
        let string = '';
        const letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'; // Include numbers if you want
        for (let i = 0; i < length; i++) {
            string += letters.charAt(Math.floor(Math.random() * letters.length));
        }
        return string
        // setTimeout(() => resolve(string), 1000);
    // });
};

D.projectNumber = helper.getRandomString(6);
D.qouteId = helper.getRandomString(5);
D.projectName = 'proj-' + helper.getRandomString(4);

const types = ['Current', 'Steel', 'Prospective', 'Archive', 'In DLP', 'Design'];
var Types = Math.floor(Math.random() * types.length);
D.type = types[Types];

D.projectPrincipal = `projPrincibal- ${setNewRandomNo(500)}`;
D.principalABN = `${setNewRandomNo(20000)}`;
D.principalAddress = 'principalAddress-' + helper.getRandomString(4);
D.localGov = 'localGov-' + helper.getRandomString(4);

const designApps = ['QUU', 'GCCC', 'Other'];
var DesignApps = Math.floor(Math.random() * designApps.length);
D.apps = designApps[DesignApps];

D.principalContactName = 'principalContactName' + helper.getRandomString(5);
D.principalContactEmail = 'principalContactEmail@' + helper.getRandomString(5) + '.com';
D.principalContactMobile = `${setNewRandomNo(9000)}`;
D.superName = 'superName' +  helper.getRandomString(5);
D.superEmail = 'superemail@' + helper.getRandomString(5) + '.com';
D.superMobile = `${setNewRandomNo(10000)}`;
D.description = 'description-' + helper.getRandomString(6);
D.siteManager = 'siteManager-' + helper.getRandomString(4);
D.siteManagerEmail = 'siteManagerEmail@' + helper.getRandomString(5) + '.com';

const constructionManagers = ['Geoff Clark', 'Testing Cons Man', 'Cons Manager', 'Bill O’Flaherty', 'Shaun Penna'];
var managers = Math.floor(Math.random() * constructionManagers.length);
D.constructionManager = constructionManagers[managers];

const contractAdmins = ['Cons Admin', 'Melissa Allen', 'Alan Benton', 'Testing Cont Admin', 'Scott Dunmore', 'David Elms'];
var admins = Math.floor(Math.random() * contractAdmins.length);
D.admin = contractAdmins[admins];

const DrafteeUsers = ['Testing Draftee', 'Michael Gaveran'];
var users = Math.floor(Math.random() * DrafteeUsers.length);
D.DrafteeUser = DrafteeUsers[users];

const teamMembers = ['John Citizen', 'Michael Gaveran', 'Peter Raspotnik', 'SpaceFrame Safety', 'Darrin Veness'];
var members = Math.floor(Math.random() * teamMembers.length);
D.teamMember = teamMembers[members];

D.doctorName = 'doctorName-' + helper.getRandomString(4);
D.doctorAddress = 'doctorAddress-' + helper.getRandomString(4);
D.doctorPhone = `${setNewRandomNo(11000)}`;
D.hospitalName = 'hospitalName-' + helper.getRandomString(4);
D.hospitalAddress = 'hospitalAddress-' + helper.getRandomString(4);
D.hospitalPhone = `${setNewRandomNo(100000)}`;

D.constructionPeriod = `${setNewRandomNo(20)}`;
D.csmp = 'csmp-' + helper.getRandomString(4);
D.msm = 'msm-' + helper.getRandomString(4);

const contractTypes = ['Purchase Order', 'Contract'];
var ContractTypes = Math.floor(Math.random() * contractTypes.length);
D.contractType = contractTypes[ContractTypes];

const statuses = ['Approved', 'Pending', 'Not Approved'];
var Statuses = Math.floor(Math.random() * statuses.length);
D.status = statuses[Statuses];

D.contractAmount = `${setNewRandomNo(50000)}`;
D.siteAddress = 'siteAddress-' + helper.getRandomString(4);

const states = ['Australian Capital Territory', 'New South Wales', 'Northern Territory', 'Queensland', 'South Australia', 'Tasmania'];
var States = Math.floor(Math.random() * states.length);
D.state = states[States];

D.postcode = `${setNewRandomNo(20000)}`;
D.lds = `${setNewRandomNo(500)}`;
D.dlp = `${setNewRandomNo(100)}`;

D.parseCSVFileToJSON = function (path, encoding) {
    const csvFile = fs.readFileSync(path, encoding);
    const parseCsv = csv => {
        const lines = csv.split('\n'); // Divide csv into lines
        const header = lines.shift().split(','); // Take out header and put in an array
        return lines.map(line => {
            const bits = line.split(','); // Take all lines without header
            const obj = {};
            header.forEach((h, i) => obj[h] = bits[i]); // or use reduce here
            return obj;
        });
    };
    return parseCsv(csvFile);
};

module.exports = D;
