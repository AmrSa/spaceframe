import frisby from 'frisby';
import qs from 'qs';
import config from '../config/shared.conf'
const mysql = require('mysql2/promise');
import DB from '../utils/DB-utils'


const c = exports;
const baseUrl = 'https://test.example.com';

c.commands = function () {
    browser.addCommand('retryFailedTest', function (int) {
        return this.retries(parseInt(value));
    });

    browser.addCommand('submitDBQuery', (query)=> {
        return DB.submitDBQuery(query)
    });

    browser.addCommand('APILogin', function (email, password) {
        return frisby
            .post(`${baseUrl}/mobile`, {
                body: qs.stringify({
                    username: email,
                    password: password,
                    grant_type: 'password'
                }),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'kick-out-user': true
                }
            })
            .expect('status', 200)
            .then(res => {
                const accessToken = JSON.parse(res.body).access_token;
                const refreshToken = JSON.parse(res.body).refresh_token;
                const tokeType = JSON.parse(res.body).token_type;
                const Authorization = tokeType + ' ' + accessToken;

                exports.auth = Authorization;
                exports.ref = refreshToken;

                return frisby
                    .post(`${baseUrl}/users/userData`, {
                        headers: {
                            refreshToken: refreshToken,
                            Authorization: Authorization
                        }
                    })
                    .expect('status', 200)
                    .then(resp => {
                        const orgId = JSON.parse(resp.body).organizationId;
                        const officeId = JSON.parse(resp.body).officeId;
                        console.log(orgId);
                        console.log(officeId);

                        frisby
                            .globalSetup({
                                request: {
                                    headers: {
                                        'Content-Type': 'application/x-www-form-urlencoded',
                                        Host: '/',
                                        Origin: baseUrl,
                                        Referer: baseUrl,
                                        access_token: accessToken,
                                        refreshToken: refreshToken,
                                        token_type: tokeType,
                                        Authorization: Authorization
                                    }
                                }
                            });
                    });
            });
    });

    browser.addCommand('customizedApiRequest', function () {
        return frisby
            .setup({
                request: {
                    headers: {
                        organizationId: 1,
                        Authorization: c.auth,
                        refreshToken: c.ref,
                    }
                }
            })

            .get(`${baseUrl}/api/cases/recent`)
            .expect('status', 403)
            .expect('bodyContains', 'put your message here');
    });

    browser.addCommand('expectListOfElementsToBeDisplayed', function (elementArray) {
        var list = elementArray;
        var element;
        for (element of list) {
            element.waitForDisplayed();
            expect(element).toBeDisplayed();
        }
    });

    browser.addCommand('forceClick', function () {
        return browser.execute((_element) => {
            _element.click();
        }, this);
    }, true);

    browser.addCommand('forceSendValue', function (text) {
        return browser.execute((_element, _text) => {
            _element.value = _text;
        }, this, text);
    }, true);
};

module.exports = c;
