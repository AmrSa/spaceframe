const { config } = require('./shared.conf');
const caps = require('../utils/caps');
const specs = require('../utils/specs');

const drivers = {
    chrome: { version: '88.0.4324.96' }, // https://chromedriver.chromium.org/
};

exports.config = {
    ...config,
    ...{
        specs: specs.specs,
        capabilities: caps.ChromeDesktopCaps,
        baseUrl: 'http://white.sf.bluesky.sh/spaceframe',
        services: [['selenium-standalone', {
            installArgs: {
                version: '3.141.59',
                baseURL: 'https://selenium-release.storage.googleapis.com',
                drivers: {
                    chrome: {
                        version: '88.0.4324.96',
                        baseURL: 'https://chromedriver.storage.googleapis.com',
                        'goog:chromeOptions': {
                            args: [
                                // 'user-data-dir=/home/amrka/.config/google-chrome/Default',
                                '--disable-web-security',
                                // '--window-size=900,600'
                                // '--disable-dev-shm-usage',
                                // '--no-sandbox',
                                // '--enable-automation',
                                // '--disable-gpu',
                                'start-maximized',
                                // '--headless',
                            ],
                        }
                    }
                }
            },
            args: {
                seleniumArgs: ['-host', 'localhost', '-port', '4455'],
                drivers
            },
        }],
        ],
    }
};
