![SpaceFrame](src/test/resources/poster.png)


**SpaceFrame Automation Framework** 
Web testing automation framework, used to perform E2e automation testing against web portal for space frame to decrease manual QA testing effort, scale the test execution and utilize QA resources


## Benifits & Features

 - Perform automation test against any web browser type.
 - Perfom API automation testing.
 - Provide detailed report with screenshot on failure.
 - Can be configured to run automation test against mobile native apps
 - Can be configured to work and interact with docker containers

### Structure & Work Process


**Component diagram**

![Diagram](src/test/resources/diag.png)



#### Dependencies & Plugin

 - Mocha : As test execution framework.
 - WDIO  : Autonation framework
 - Selenium : Web driver
 - Frisby : API automation tool



*Amr Kamel* 